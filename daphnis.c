/*
    daphnis - v4l2 driver for the Multimedia eXtension Board

    Copyright (C) 1998-2006 Michael Hunold <michael@mihu.de>

    Visit http://www.mihu.de/linux/saa7146/daphnis/
    for further details about this card.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#define DEBUG_VARIABLE debug

#include <media/saa7146_vv.h>
#include <media/v4l2-common.h>
#include <media/saa7115.h>


#define I2C_SAA7111A  0x24
#define DAPHNIS_BOARD_CAN_DO_VBI(dev)   (dev->revision != 0)

/* global variable */
static int daphnis_num;


static int debug=0;
module_param(debug, int, 0);
MODULE_PARM_DESC(debug, "debug verbosity(default 0)");



#define DAPHNIS_INPUTS	sizeof(daphnis_inputs)/sizeof(struct v4l2_input)
static struct v4l2_input daphnis_inputs[] = {
  { 0, "Port A",	V4L2_INPUT_TYPE_CAMERA,	2, 0, V4L2_STD_PAL_BG|V4L2_STD_NTSC_M, 0 },
  { 1, "Port B",	V4L2_INPUT_TYPE_CAMERA,	2, 0, V4L2_STD_PAL_BG|V4L2_STD_NTSC_M, 0 },
};

enum {
  CUSTOM_CID_LIGHT = V4L2_CID_PRIVATE_BASE,
  CUSTOM_CID_SAA7111_PORT,
  CUSTOM_CID_SET_SAT,
  CUSTOM_CID_HPS_V,
  CUSTOM_CID_END
};

#define DAPHNIS_CONTROLS sizeof(daphnis_controls)/sizeof(struct v4l2_queryctrl)
//#define DAPHNIS_CONTROLS 4
static struct v4l2_queryctrl daphnis_controls[] = {
  { CUSTOM_CID_LIGHT, V4L2_CTRL_TYPE_BOOLEAN, "Light Control", 0, 1, 1, 0, 0 },
  { CUSTOM_CID_SAA7111_PORT, V4L2_CTRL_TYPE_BOOLEAN, "saa7111 port", 0, 1, 1, 0, 0 },
  { CUSTOM_CID_SET_SAT, V4L2_CTRL_TYPE_INTEGER, "Set Saturation", 0, 255, 1, 0, 0 },
  { CUSTOM_CID_HPS_V, V4L2_CTRL_TYPE_INTEGER, "HPS v", 0, 0xffffffff, 1, 0, 0 }
};

struct daphnis
{
  struct video_device	*video_dev;
  struct video_device	*vbi_dev;
  
  struct i2c_adapter	i2c_adapter;
  struct v4l2_subdev	*saa7111a;
  
  int cur_input;	/* current input */
};


#define saa7111a_call(daphnis, o, f, args...) \
	v4l2_subdev_call(daphnis->saa7111a, o, f, ##args)
#define call_all(dev, o, f, args...) \
	v4l2_device_call_until_err(&dev->v4l2_dev, 0, o, f, ##args)


static struct saa7146_extension extension;

static int daphnis_probe(struct saa7146_dev *dev)
{

    struct daphnis *daphnis = NULL;
      
    printk("START daphnis_probe\n");
    daphnis = kzalloc(sizeof(struct daphnis), GFP_KERNEL);
    if (daphnis == NULL) {
	    DEB_D(("not enough kernel memory.\n"));
	    return -ENOMEM;
    }

    snprintf(daphnis->i2c_adapter.name, sizeof(daphnis->i2c_adapter.name), "daphnis%d", daphnis_num);

    saa7146_i2c_adapter_prepare(dev, &daphnis->i2c_adapter, SAA7146_I2C_BUS_BIT_RATE_480);
    if (i2c_add_adapter(&daphnis->i2c_adapter) < 0) {
	    DEB_S(("cannot register i2c-device. skipping.\n"));
	    kfree(daphnis);
	    return -EFAULT;
    }

    daphnis->saa7111a = v4l2_i2c_new_subdev(&dev->v4l2_dev, &daphnis->i2c_adapter,"saa7115", "saa7111", I2C_SAA7111A,NULL);


    /* check if all devices are present */
    if ( !daphnis->saa7111a ) {
	    printk("daphnis: did not find all i2c devices. aborting\n");
	    i2c_del_adapter(&daphnis->i2c_adapter);
	    kfree(daphnis);
	    return -ENODEV;
    }

    /* all devices are present, probe was successful */
    /* all devices are present, probe was successful */
    DEB_D(("dpc_v4l2.o: daphnis_probe succeeded for this device.\n"));

    /* we store the pointer in our private data field */
    dev->ext_priv = daphnis;
    printk("END daphnis_probe\n");
    return 0;
}


/* bring hardware to a sane state. this has to be done, just in case someone
   wants to capture from this device before it has been properly initialized.
   the capture engine would badly fail, because no valid signal arrives on the
   saa7146, thus leading to timeouts and stuff. */
static int daphnis_init_done(struct saa7146_dev* dev)
{
	struct daphnis* daphnis = (struct daphnis*)dev->ext_priv;
	struct i2c_client *client = v4l2_get_subdevdata(daphnis->saa7111a);
	//struct i2c_msg msg;
	v4l2_std_id std = V4L2_STD_PAL_BG;

	/* select video mode in saa7111a */
	saa7111a_call(daphnis, core, s_std, std);

	/* this is ugly, but because of the fact that this is completely
	   hardware dependend, it should be done directly... */
	saa7146_write(dev, MC1, (MASK_08 | MASK_24 | MASK_10 | MASK_26));

	saa7146_write(dev, DD1_STREAM_B,	0x00000000);
	saa7146_write(dev, DD1_INIT,		0x02000200);
	saa7146_write(dev, MC2, (MASK_09 | MASK_25 | MASK_10 | MASK_26));

	i2c_smbus_write_byte_data(client, 0x00, 0x11);
	i2c_smbus_write_byte_data(client, 0x02, 0xc5);
	i2c_smbus_write_byte_data(client, 0x03, 0x30);
	i2c_smbus_write_byte_data(client, 0x04, 0x00);
	i2c_smbus_write_byte_data(client, 0x05, 0x00);
	i2c_smbus_write_byte_data(client, 0x06, 0xde);
	i2c_smbus_write_byte_data(client, 0x07, 0xad);
	i2c_smbus_write_byte_data(client, 0x08, 0xa8);
	i2c_smbus_write_byte_data(client, 0x09, 0x03);
	i2c_smbus_write_byte_data(client, 0x0a, 0x70);
	i2c_smbus_write_byte_data(client, 0x0b, 0x40);
	i2c_smbus_write_byte_data(client, 0x0c, 0x48);
	i2c_smbus_write_byte_data(client, 0x0d, 0x00);
	i2c_smbus_write_byte_data(client, 0x0e, 0x03);
	
	i2c_smbus_write_byte_data(client, 0x10, 0xd0);
	i2c_smbus_write_byte_data(client, 0x11, 0x1c);
	i2c_smbus_write_byte_data(client, 0x12, 0x01);
	i2c_smbus_write_byte_data(client, 0x13, 0x30);
	
	i2c_smbus_write_byte_data(client, 0x1f, 0x81);

	return 0;
}


static int vidioc_queryctrl(struct file *file, void *fh, struct v4l2_queryctrl *qc)
{
	struct saa7146_dev *dev = ((struct saa7146_fh *)fh)->dev;
	int i;

	for (i = DAPHNIS_CONTROLS - 1; i >= 0; i--) {
		if (daphnis_controls[i].id == qc->id) {
			*qc = daphnis_controls[i];
			DEB_D(("VIDIOC_QUERYCTRL %d.\n", qc->id));
			return 0;
		}
	}
	return dev->ext_vv_data->core_ops->vidioc_queryctrl(file, fh, qc);
}

static int vidioc_g_ctrl(struct file *file, void *fh, struct v4l2_control *vc)
{
	struct saa7146_dev *dev = ((struct saa7146_fh *)fh)->dev;
//	struct daphnis *daphnis = (struct daphnis *)dev->ext_priv;
	int i;

	for (i = DAPHNIS_CONTROLS - 1; i >= 0; i--) {
		if (daphnis_controls[i].id == vc->id)
			break;
	}

	if (i < 0)
		return dev->ext_vv_data->core_ops->vidioc_g_ctrl(file, fh, vc);

	switch( vc->id ){
	  case CUSTOM_CID_LIGHT:
		 vc->value = saa7146_read( dev, GPIO_CTRL ) == SAA7146_GPIO_OUTHI;
		 DEB_D(("dpc_v4l2.o: v4l2_ioctl: VIDIOC_G_CONTROL CUSTOM_CID_LIGHT %d.\n",CUSTOM_CID_LIGHT));
		 return 0;
		 
	  case CUSTOM_CID_HPS_V:
		 vc->value = saa7146_read( dev, HPS_V_SCALE );
		 return 0;
		 

	}
	
	return 0;
}

static int vidioc_s_ctrl(struct file *file, void *fh, struct v4l2_control *vc)
{
	struct saa7146_dev *dev = ((struct saa7146_fh *)fh)->dev;
	struct daphnis *daphnis = (struct daphnis *)dev->ext_priv;
	struct i2c_client *client = v4l2_get_subdevdata(daphnis->saa7111a);
	int i = 0;

	for (i = DAPHNIS_CONTROLS - 1; i >= 0; i--) {
		if (daphnis_controls[i].id == vc->id)
			break;
	}

	if (i < 0)
		return dev->ext_vv_data->core_ops->vidioc_s_ctrl(file, fh, vc);

	switch( vc->id ){
	  case CUSTOM_CID_LIGHT:
	  {
		 saa7146_write( dev, GPIO_CTRL, vc->value ? SAA7146_GPIO_OUTHI : SAA7146_GPIO_OUTLO );
		 DEB_D(("dpc_v4l2.o: v4l2_ioctl: VIDIOC_S_CONTROL CUSTOM_CID_LIGHT %d.\n",CUSTOM_CID_LIGHT));
		 return 0;
	  }
	  
	  case CUSTOM_CID_SAA7111_PORT:
	  {
		 if( vc->value ){
			i2c_smbus_write_byte_data(client, 0x02, 0xc0);
		 }else{
			i2c_smbus_write_byte_data(client, 0x02, 0xc5);
		 }
		 DEB_D(("dpc_v4l2.o: v4l2_ioctl: VIDIOC_S_CONTROL CUSTOM_CID_SAA7111_PORT %d.\n",vc->value));
		 return 0;
	  }
	  
	  case CUSTOM_CID_SET_SAT:
	  {
		 //i2c_smbus_write_byte_data(daphnis->saa7111a, 0x0c, vc->value & 0xff );
		 vc->id = V4L2_CID_SATURATION;
		 saa7111a_call(daphnis, core, s_ctrl, vc);
		 DEB_D(("dpc_v4l2.o: v4l2_ioctl: VIDIOC_S_CONTROL CUSTOM_CID_SET_SAT %d.\n",vc->value));
		 return 0;
	  }
	  
	  case CUSTOM_CID_HPS_V:
	  {
		 saa7146_write( dev, HPS_V_SCALE, vc->value );
		 saa7146_write( dev, MC2, (MASK_05 | MASK_06 | MASK_21 | MASK_22) );
		 return 0;
	  }
	}
	
	return 0;
}

static int vidioc_enum_input(struct file *file, void *fh, struct v4l2_input *i)
{
	DEB_EE(("VIDIOC_ENUMINPUT %d.\n", i->index));
	if (i->index >= DAPHNIS_INPUTS)
		return -EINVAL;
	memcpy(i, &daphnis_inputs[i->index], sizeof(struct v4l2_input));
	return 0;
}

static int vidioc_g_input(struct file *file, void *fh, unsigned int *i)
{
	struct saa7146_dev *dev = ((struct saa7146_fh *)fh)->dev;
	struct daphnis *daphnis = (struct daphnis *)dev->ext_priv;
	*i = daphnis->cur_input;

	DEB_EE(("VIDIOC_G_INPUT %d.\n", *i));
	return 0;
}

static int vidioc_s_input(struct file *file, void *fh, unsigned int input)
{
	struct saa7146_dev *dev = ((struct saa7146_fh *)fh)->dev;
	struct daphnis *daphnis = (struct daphnis *)dev->ext_priv;

	DEB_EE(("VIDIOC_S_INPUT %d.\n", input));

	if (input < 0 || input >= DAPHNIS_INPUTS)
		return -EINVAL;

	daphnis->cur_input = input;
	return 0;
}

static struct saa7146_ext_vv vv_data;

/* this function only gets called when the probing was successful */
static int daphnis_attach(struct saa7146_dev *dev, struct saa7146_pci_extension_data *info)
{
	struct daphnis *daphnis = (struct daphnis *)dev->ext_priv;
   printk("Debug=%d\n", debug);
	DEB_EE(("dev:%p\n", dev));

	/* checking for i2c-devices can be omitted here, because we
	   already did this in "daphnis_vl42_probe" */

	saa7146_vv_init(dev, &vv_data);
	vv_data.ops.vidioc_queryctrl = vidioc_queryctrl;
	vv_data.ops.vidioc_g_ctrl = vidioc_g_ctrl;
	vv_data.ops.vidioc_s_ctrl = vidioc_s_ctrl;
	vv_data.ops.vidioc_enum_input = vidioc_enum_input;
	vv_data.ops.vidioc_g_input = vidioc_g_input;
	vv_data.ops.vidioc_s_input = vidioc_s_input;

	if (saa7146_register_device(&daphnis->video_dev, dev, "daphnis", VFL_TYPE_GRABBER)) {
		ERR(("cannot register capture v4l2 device. skipping.\n"));
		return -1;
	}

	/* initialization stuff (vbi) (only for revision > 0 and for extensions which want it)*/
	if (DAPHNIS_BOARD_CAN_DO_VBI(dev)) {
		if (saa7146_register_device(&daphnis->vbi_dev, dev, "daphnis", VFL_TYPE_VBI)) {
			ERR(("cannot register vbi v4l2 device. skipping.\n"));
		}
	}

	printk("daphnis: found Multimedia eXtension Board #%d.\n", daphnis_num);

	daphnis_num++;
	daphnis_init_done(dev);
	return 0;
}

static int daphnis_detach(struct saa7146_dev *dev)
{
	struct daphnis *daphnis = (struct daphnis *)dev->ext_priv;

	DEB_EE(("dev:%p\n", dev));

	saa7146_unregister_device(&daphnis->video_dev,dev);
	if (DAPHNIS_BOARD_CAN_DO_VBI(dev))
		saa7146_unregister_device(&daphnis->vbi_dev, dev);
	saa7146_vv_release(dev);

	daphnis_num--;

	i2c_del_adapter(&daphnis->i2c_adapter);
	kfree(daphnis);

	return 0;
}

static int std_callback(struct saa7146_dev *dev, struct saa7146_standard *standard)
{
	//struct daphnis *daphnis = (struct daphnis *)dev->ext_priv;
	printk("DAPHNIS - std_Callback\n");
	return 0;
}

static struct saa7146_standard standard[] = {
	{
		.name	= "PAL-BG", 	.id	= V4L2_STD_PAL_BG,
		.v_offset	= 0x17,	.v_field 	= 288,
		.h_offset	= 0x14,	.h_pixels 	= 680,
		.v_max_out	= 576,	.h_max_out	= 768,
	}, {
		.name	= "NTSC", 	.id	= V4L2_STD_NTSC,
		.v_offset	= 0x16,	.v_field 	= 240,
		.h_offset	= 0x06,	.h_pixels 	= 708,
		.v_max_out	= 480,	.h_max_out	= 640,
	}, {
		.name	= "SECAM", 	.id	= V4L2_STD_SECAM,
		.v_offset	= 0x14,	.v_field 	= 288,
		.h_offset	= 0x14,	.h_pixels 	= 720,
		.v_max_out	= 576,	.h_max_out	= 768,
	}
};

static struct saa7146_pci_extension_data daphnis = {
	.ext_priv = "Multimedia eXtension Board",
	.ext = &extension,
};

static struct pci_device_id pci_tbl[] = {
	{
		.vendor    = PCI_VENDOR_ID_PHILIPS,
		.device	   = PCI_DEVICE_ID_PHILIPS_SAA7146,
		.subvendor = 0x0000,
		.subdevice = 0x0000,
		.driver_data = (unsigned long)&daphnis,
	}, {
		.vendor	= 0,
	}
};

MODULE_DEVICE_TABLE(pci, pci_tbl);

static struct saa7146_ext_vv vv_data = {
	.inputs		= DAPHNIS_INPUTS,
	.capabilities	=  V4L2_CAP_VBI_CAPTURE,
	.stds		= &standard[0],
	.num_stds	= sizeof(standard)/sizeof(struct saa7146_standard),
	.std_callback	= &std_callback,
};

static struct saa7146_extension extension = {
	.name		= "Daphnis Video Grabber",
	//.flags		= SAA7146_USE_I2C_IRQ,
	.flags= 0,

	.pci_tbl= &pci_tbl[0],
	.module	= THIS_MODULE,

	.probe	= daphnis_probe,
	.attach	= daphnis_attach,
	.detach	= daphnis_detach,

	.irq_mask	= 0,
	.irq_func	= NULL,
};

static int __init daphnis_init_module(void)
{
  if (saa7146_register_extension(&extension)) {
	DEB_S(("failed to register extension.\n"));
	return -ENODEV;
  }
  printk("After register extension\n");
  return 0;
}

static void __exit daphnis_cleanup_module(void)
{
	saa7146_unregister_extension(&extension);
}

module_init(daphnis_init_module);
module_exit(daphnis_cleanup_module);

MODULE_DESCRIPTION("video4linux-2 driver for the Daphnis Video Grabber");
MODULE_AUTHOR("Adrian DARABANT <asergiu@yahoo.co.uk>");
MODULE_LICENSE("GPL");
