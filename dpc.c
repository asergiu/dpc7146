/*
daphnis.c - v4l2 driver for the dpc7146 demonstration board

Copyright (C) 2000-2003 Michael Hunold <michael@mihu.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#define DEBUG_VARIABLE debug

#include <media/saa7146_vv.h>
#include <linux/version.h>

//#include <media/saa7115.h>

#define I2C_SAA7111A            0x24

#define DPC_BOARD_CAN_DO_VBI(dev)   (dev->revision != 0)

enum {
  CUSTOM_CID_LIGHT = V4L2_CID_PRIVATE_BASE, 
  CUSTOM_CID_SAA7111_PORT,
  CUSTOM_CID_SET_SAT, 
  CUSTOM_CID_HPS_V,
  CUSTOM_CID_END
};

#define DPC_CONTROLS sizeof(dpc7146_controls)/sizeof(struct v4l2_queryctrl)
//#define DPC_CONTROLS 4
static struct v4l2_queryctrl dpc7146_controls[] = {
  { CUSTOM_CID_LIGHT, V4L2_CTRL_TYPE_BOOLEAN, "Light Control", 0, 1, 1, 0, 0 },
  { CUSTOM_CID_SAA7111_PORT, V4L2_CTRL_TYPE_BOOLEAN, "saa7111 port", 0, 1, 1, 0, 0 },
  { CUSTOM_CID_SET_SAT, V4L2_CTRL_TYPE_INTEGER, "Set Saturation", 0, 255, 1, 0, 0 },
  { CUSTOM_CID_HPS_V, V4L2_CTRL_TYPE_INTEGER, "HPS v", 0, 0xffffffff, 1, 0, 0 }
};


static int debug;
module_param(debug, int, 0);
MODULE_PARM_DESC(debug, "debug verbosity");

static int dpc_num;

#define DPC_INPUTS sizeof(dpc_inputs)/sizeof(struct v4l2_input)
//#define DPC_INPUTS	2
static struct v4l2_input dpc_inputs[] = {
  { 0, "Port A",	V4L2_INPUT_TYPE_CAMERA,	2, 0, V4L2_STD_PAL_BG|V4L2_STD_NTSC_M, 0 },
  { 1, "Port B",	V4L2_INPUT_TYPE_CAMERA,	2, 0, V4L2_STD_PAL_BG|V4L2_STD_NTSC_M, 0 },
};


#define DPC_AUDIOS	0

struct dpc
{
  struct video_device	*video_dev;
  struct i2c_adapter	i2c_adapter;
  
  struct video_device	*vbi_dev;

  int cur_input;	/* current input */
};


struct saa7111_data
{
	s8 addr;
	u8 byte;
};



/* fixme: add vbi stuff here */
static int dpc_probe(struct saa7146_dev* dev)
{
  struct dpc* dpc = NULL;
  
  DEB_S(("dpc7146 dpc_probe\n"));
  
  dpc = kzalloc(sizeof(struct dpc), GFP_KERNEL);
  if( NULL == dpc ) {
    printk("dpc_v4l2.o: dpc_probe: not enough kernel memory.\n");
    return -ENOMEM;
  }
  
  /* FIXME: enable i2c-port pins, video-port-pins
  video port pins should be enabled here ?! */
  saa7146_write(dev, MC1, (MASK_08 | MASK_24 | MASK_10 | MASK_26));
  
  //dadi from hexium orion
  saa7146_write(dev, DD1_INIT, 0x01000100);
  saa7146_write(dev, DD1_STREAM_B, 0x00000000);
  saa7146_write(dev, MC2, (MASK_09 | MASK_25 | MASK_10 | MASK_26));
  //dadi END - missing init sequence - have to modprove saa7111 manually though
  
  dpc->i2c_adapter = (struct i2c_adapter) {
 #if LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,36)
    .class = I2C_CLASS_TV_ANALOG,
 #endif   
    .name = "dpc",
  };
  saa7146_i2c_adapter_prepare(dev, &dpc->i2c_adapter, SAA7146_I2C_BUS_BIT_RATE_480);
  if(i2c_add_adapter(&dpc->i2c_adapter) < 0) {
    DEB_S(("cannot register i2c-device -adapter. skipping.\n"));
    kfree(dpc);
    return -EFAULT;
  }
  
  /* we store the pointer in our private data field */
  dev->ext_priv = dpc;
  
  return 0;
}

static struct saa7111_data saa7111_init[] = {
  {0x00, 0x11}, 
  {0x02, 0xc5}, 
  {0x03, 0x30},
  {0x04, 0x00},
  {0x05, 0x00},
  {0x06, 0xde},
  {0x07, 0xad},
  {0x08, 0xa8},
  {0x09, 0x03},
  {0x0a, 0x70},
  {0x0b, 0x40},
  {0x0c, 0x48},
  {0x0d, 0x00},
  {0x0e, 0x03},
  {0x10, 0xd0},
  {0x11, 0x1c},
  {0x12, 0x01},
  {0x13, 0x30},
  {0x1f, 0x81}
};
  

/* bring hardware to a sane state. this has to be done, just in case someone
wants to capture from this device before it has been properly initialized.
the capture engine would badly fail, because no valid signal arrives on the
saa7146, thus leading to timeouts and stuff. */
static int dpc_init_done(struct saa7146_dev* dev)
{
  struct dpc* dpc = (struct dpc*)dev->ext_priv;
  
  DEB_D(("dpc_v4l2.o: dpc_init_done called.\n"));
  
  /* initialize the helper ics to useful values */
   union i2c_smbus_data data;
   int i=0;
   for (i = 0; i < sizeof(saa7111_init) / sizeof(struct saa7111_data); i++) {
	data.byte = saa7111_init[i].byte;
	if (0 != i2c_smbus_xfer(&dpc->i2c_adapter, I2C_SAA7111A, 0, I2C_SMBUS_WRITE, saa7111_init[i].addr, I2C_SMBUS_BYTE_DATA, &data)) {
	    printk("dpc: dpc_init_done() failed for address 0x%02x\n", saa7111_init[i].addr);
	}
  }
  return 0;
}


static int vidioc_enum_input(struct file *file, void *fh, struct v4l2_input *i)
{
  DEB_EE(("VIDIOC_ENUMINPUT %d.\n", i->index));
  
  if (i->index >= DPC_INPUTS)
    return -EINVAL;
  
  memcpy(i, &dpc_inputs[i->index], sizeof(struct v4l2_input));
  
  DEB_D(("v4l2_ioctl: VIDIOC_ENUMINPUT %d.\n", i->index));
  return 0;
}

static int vidioc_g_input(struct file *file, void *fh, unsigned int *input)
{
  struct saa7146_dev *dev = ((struct saa7146_fh *)fh)->dev;
  struct dpc *dpc = (struct dpc *) dev->ext_priv;
  
  *input = dpc->cur_input;
  
  DEB_D(("VIDIOC_G_INPUT: %d\n", *input));
  return 0;
}

static int vidioc_s_input(struct file *file, void *fh, unsigned int input)
{
  struct saa7146_dev *dev = ((struct saa7146_fh *)fh)->dev;
  struct dpc *dpc = (struct dpc *) dev->ext_priv;
  
  DEB_EE(("VIDIOC_S_INPUT %d.\n", input));
  
  if (input < 0 || input >= DPC_INPUTS)
    return -EINVAL;
  
  dpc->cur_input = input;
  return 0;
}

/* the saa7146 provides some controls (brightness, contrast, saturation)
which gets registered *after* this function. because of this we have
to return with a value != 0 even if the function succeded.. */
static int vidioc_queryctrl(struct file *file, void *fh, struct v4l2_queryctrl *qc)
{
  struct saa7146_dev *dev = ((struct saa7146_fh *)fh)->dev;
  int i;
  
  for (i = DPC_CONTROLS - 1; i >= 0; i--) {
    if (dpc7146_controls[i].id == qc->id) {
      *qc = dpc7146_controls[i];
      DEB_D(("VIDIOC_QUERYCTRL %d.\n", qc->id));
      return 0;
    }
  }
  return dev->ext_vv_data->core_ops->vidioc_queryctrl(file, fh, qc);
}

static int vidioc_g_ctrl(struct file *file, void *fh, struct v4l2_control *vc)
{
  struct saa7146_dev *dev = ((struct saa7146_fh *)fh)->dev;
  int i;
  
  for (i = DPC_CONTROLS - 1; i >= 0; i--) {
    if (dpc7146_controls[i].id == vc->id)
      break;
  }
  
  if (i < 0)
    return dev->ext_vv_data->core_ops->vidioc_g_ctrl(file, fh, vc);
  
  
  switch( vc->id ){
    case CUSTOM_CID_LIGHT:
      vc->value = saa7146_read( dev, GPIO_CTRL ) == SAA7146_GPIO_OUTHI;
      DEB_D(("dpc_v4l2.o: v4l2_ioctl: VIDIOC_G_CONTROL CUSTOM_CID_LIGHT %d.\n",vc->value));
      return 0;
      
    case CUSTOM_CID_HPS_V:
      vc->value = saa7146_read( dev, HPS_V_SCALE );
      DEB_D(("dpc_v4l2.o: v4l2_ioctl: VIDIOC_G_CONTROL CUSTOM_CID_HPS_V %d.\n",vc->value));
      return 0;
  }
  
  return 0;
}

static int vidioc_s_ctrl(struct file *file, void *fh, struct v4l2_control *vc)
{
  struct saa7146_dev *dev = ((struct saa7146_fh *)fh)->dev;
  struct dpc *dpc = (struct dpc *) dev->ext_priv;
  union i2c_smbus_data data;
  
  int i = 0;
  
  for (i = DPC_CONTROLS - 1; i >= 0; i--) {
    if (dpc7146_controls[i].id == vc->id)
      break;
  }
  
  if (i < 0)
    return dev->ext_vv_data->core_ops->vidioc_s_ctrl(file, fh, vc);
  
  switch( vc->id ){
    case CUSTOM_CID_LIGHT:
    {
      saa7146_write( dev, GPIO_CTRL, vc->value ? SAA7146_GPIO_OUTHI : SAA7146_GPIO_OUTLO );
      DEB_D(("dpc_v4l2.o: v4l2_ioctl: VIDIOC_S_CONTROL CUSTOM_CID_LIGHT %d.\n",vc->value));
      return 0;
    }
    
    case CUSTOM_CID_SAA7111_PORT:
    {
      if( vc->value ){
	//--i2c_smbus_write_byte_data(dpc->saa7111a, 0x02, 0xc0);
	data.byte=0xc0;
	if (0 != i2c_smbus_xfer(&dpc->i2c_adapter, I2C_SAA7111A, 0, I2C_SMBUS_WRITE, 0x02, I2C_SMBUS_BYTE_DATA, &data))
	  printk("dpc: dpc saa7111_port failed for address 0x%02x\n", 0x02);
      }else{
	//--i2c_smbus_write_byte_data(dpc->saa7111a, 0x02, 0xc5);
	data.byte=0xc5;
	if (0 != i2c_smbus_xfer(&dpc->i2c_adapter, I2C_SAA7111A, 0, I2C_SMBUS_WRITE, 0x02, I2C_SMBUS_BYTE_DATA, &data))
	  printk("dpc: dpc saa7111_port failed for address 0x%02x\n", 0x02);
      }
      DEB_D(("dpc_v4l2.o: v4l2_ioctl: VIDIOC_S_CONTROL CUSTOM_CID_SAA7111_PORT %d.\n",vc->value));
      return 0;
    }
    
    case CUSTOM_CID_SET_SAT:
    {
      //--i2c_smbus_write_byte_data(dpc->saa7111a, 0x0c, vc->value & 0xff );
      data.byte = vc->value & 0xff;
	if (0 != i2c_smbus_xfer(&dpc->i2c_adapter, I2C_SAA7111A, 0, I2C_SMBUS_WRITE, 0x0c, I2C_SMBUS_BYTE_DATA, &data))
	  printk("dpc: dpc saa7111_port failed for address 0x%02x\n", 0x02);
	
      DEB_D(("dpc_v4l2.o: v4l2_ioctl: VIDIOC_S_CONTROL CUSTOM_CID_SET_SAT %d.\n",vc->value));
      return 0;
    }
    
    case CUSTOM_CID_HPS_V:
    {
      saa7146_write( dev, HPS_V_SCALE, vc->value );
      saa7146_write( dev, MC2, (MASK_05 | MASK_06 | MASK_21 | MASK_22) );
      DEB_D(("dpc_v4l2.o: v4l2_ioctl: VIDIOC_S_CONTROL CUSTOM_CID_HPS_V %d.\n",vc->value));
      return 0;
    }
  } 
  
  return 0;
}


static struct saa7146_ext_vv vv_data;

/* this function only gets called when the probing was successful */
static int dpc_attach(struct saa7146_dev* dev, struct saa7146_pci_extension_data *info)
{
  struct dpc* dpc = (struct dpc*)dev->ext_priv;
  
  DEB_D(("dpc_v4l2.o: dpc_attach called.\n"));
  
  /* checking for i2c-devices can be omitted here, because we
  already did this in "dpc_vl42_probe" */
  
  saa7146_vv_init(dev,&vv_data);
  vv_data.ops.vidioc_queryctrl = vidioc_queryctrl;
  vv_data.ops.vidioc_g_ctrl = vidioc_g_ctrl;
  vv_data.ops.vidioc_s_ctrl = vidioc_s_ctrl;
  vv_data.ops.vidioc_enum_input = vidioc_enum_input;
  vv_data.ops.vidioc_g_input = vidioc_g_input;
  vv_data.ops.vidioc_s_input = vidioc_s_input;

  
  if( 0 != saa7146_register_device(&dpc->video_dev, dev, "dpc", VFL_TYPE_GRABBER)) {
    ERR(("cannot register capture v4l2 device. skipping.\n"));
    return -1;
  }
  
  /* initialization stuff (vbi) (only for revision > 0 and for extensions which want it)*/
  if( 0 != DPC_BOARD_CAN_DO_VBI(dev)) {
    if( 0 != saa7146_register_device(&dpc->vbi_dev, dev, "dpc", VFL_TYPE_VBI)) {
      ERR(("cannot register vbi v4l2 device. skipping.\n"));
    }
  }
  
//   i2c_use_client(dpc->saa7111a);
  
  printk("dpc: found 'daphnis demonstration board'-%d.\n",dpc_num);
  dpc_num++;
  
  /* the rest */
  dpc->cur_input = 0;
  dpc_init_done(dev);
  
  return 0;
}

static int dpc_detach(struct saa7146_dev* dev)
{
  struct dpc* dpc = (struct dpc*)dev->ext_priv;
  
  DEB_EE(("dev:%p\n",dev));
  
//   i2c_release_client(dpc->saa7111a);
  
  saa7146_unregister_device(&dpc->video_dev,dev);
  if( 0 != DPC_BOARD_CAN_DO_VBI(dev)) {
    saa7146_unregister_device(&dpc->vbi_dev,dev);
  }
  saa7146_vv_release(dev);
  
  dpc_num--;
  
  i2c_del_adapter(&dpc->i2c_adapter);
  kfree(dpc);
  return 0;
}

static int std_callback(struct saa7146_dev* dev, struct saa7146_standard *std)
{
  return 0;
}



static struct saa7146_extension extension;

static struct saa7146_pci_extension_data dpc = {
  .ext_priv = "Daphnis Multimedia Card",
  .ext = &extension,
};

static struct pci_device_id pci_tbl[] = {
  {
    .vendor    = PCI_VENDOR_ID_PHILIPS,
    .device	   = PCI_DEVICE_ID_PHILIPS_SAA7146,
    .subvendor = 0x0000,
    .subdevice = 0x0000,
    .driver_data = (unsigned long)&dpc,
  },
  {
    .vendor = 0,
  }
};

MODULE_DEVICE_TABLE(pci, pci_tbl);

static struct saa7146_standard dpc_standard[] = {
  {
    .name	= "PAL", 	.id	= V4L2_STD_PAL,
    .v_offset	= 0x17,	.v_field 	= 288,
    .h_offset	= 0x14,	.h_pixels 	= 680,
    .v_max_out	= 576,	.h_max_out	= 768,
  }, {
    .name	= "NTSC", 	.id	= V4L2_STD_NTSC,
    .v_offset	= 0x16,	.v_field 	= 240,
    .h_offset	= 0x06,	.h_pixels 	= 708,
    .v_max_out	= 480,	.h_max_out	= 640,
  }, {
    .name	= "SECAM", 	.id	= V4L2_STD_SECAM,
    .v_offset	= 0x14,	.v_field 	= 288,
    .h_offset	= 0x14,	.h_pixels 	= 720,
    .v_max_out	= 576,	.h_max_out	= 768,
  }
};

static struct saa7146_ext_vv vv_data = {
  .inputs	= DPC_INPUTS,
  .capabilities	= V4L2_CAP_VBI_CAPTURE,
  .stds		= &dpc_standard[0],
  .num_stds	= 3,
  .std_callback	= &std_callback,
};


static struct saa7146_extension extension = {
  .name		= "dpc7146 demonstration board",
  //.flags		= SAA7146_USE_I2C_IRQ,
  .flags = 0,
  
  
  .pci_tbl	= &pci_tbl[0],
  .module	= THIS_MODULE,
  
  .probe	= dpc_probe,
  .attach	= dpc_attach,
  .detach	= dpc_detach,
  
  .irq_mask	= 0,
  .irq_func	= NULL,
};

static int __init dpc_init_module(void)
{
  DEB_S(("\n"));
  if( 0 != saa7146_register_extension(&extension)) {
    DEB_S(("failed to register extension.\n"));
    return -ENODEV;
  }
  
  return 0;
}

static void __exit dpc_cleanup_module(void)
{
  saa7146_unregister_extension(&extension);
}

module_init(dpc_init_module);
module_exit(dpc_cleanup_module);

MODULE_DESCRIPTION("video4linux-2 driver for Daphnis cards-videograbbers");
MODULE_AUTHOR("Adrian Sergiu DARABANT <asergiu@yahoo.co.uk>");
MODULE_LICENSE("GPL");
