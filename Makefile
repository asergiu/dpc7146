ifneq ($(KERNELRELEASE),)
# We were called by kbuild

#obj-m += dpc7146-old2627.o
#obj-m += daphnis.o
obj-m += dpc.o

else  # We were called from command line

#KDIR := /lib/modules/$(shell uname -r)/build
KDIR := /usr/src/32bit

default:
	$(MAKE) -C $(KDIR) M=$(PWD) modules

clean:
	make -C $(KDIR) M=$(PWD) clean

endif  # End kbuild check
